package main.evaluator;

import main.genome.Genome;
import main.genome.GenomeEvaluator;
import main.species.Species;

import java.util.List;

public abstract class SimpleEvaluator implements GenomeEvaluator {

    @Override
    public void evaluateAll(List<Species> species) {
        for (Species s : species) {
            for(Genome g : s.getGenomes()) {
                g.setFitness(evaluate(g));
            }
        }
    }

    /**
     * @param genome Genome to evaluate.
     * @return A float value. The bigger, the better.
     */
    public abstract float evaluate(Genome genome);
}
