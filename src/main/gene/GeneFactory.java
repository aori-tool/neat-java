package main.gene;

/**
 * The gene factory keeps track of innovation numbers.
 */
public class GeneFactory {

    /**
     * Keeps track of the connection innovation. Increased by 1 at every connection creation.
     */
    private int connectionCounter = 0;

    /**
     * Keeps track of the node id. Increased by 1 at every node creation.
     */
    private int nodeCounter = 0;

    /**
     * Creates connection gene with innovation number starting from 1 and increasing for every creation.
     * Expressed is always true.
     */
    public ConnectionGene createConnection(int in, int out, float weight) {
        return new ConnectionGene(in, out, weight, true, ++connectionCounter);
    }

    /**
     * Create a node, id is assigned by a counter.
     */
    public NodeGene createNode(NodeGene.Type type) {
        return new NodeGene(type, ++nodeCounter);
    }

    // GETTERS & SETTERS
    public int getConnectionCounter() {
        return connectionCounter;
    }

    public void setConnectionCounter(int connectionCounter) {
        this.connectionCounter = connectionCounter;
    }

    public int getNodeCounter() {
        return nodeCounter;
    }

    public void setNodeCounter(int nodeCounter) {
        this.nodeCounter = nodeCounter;
    }
}

