package main.genome;

import main.species.Species;

import java.util.List;

/**
 * The Evaluator is here to determine whether a genome is good or not in comparison of all other genomes
 */
public interface GenomeEvaluator {

    /**
     * Evaluate all the genomes and sets the fitness score
     * @param species The species to evaluate
     */
    void evaluateAll(List<Species> species);
}
