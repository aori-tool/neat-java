package main.genome;

import main.gene.NodeGene;

/**
 * The validator is here to make sure all the nodes is not empty and if it's a hidden node then it verifies that a connection is done
 */
public class GenomeValidator {

    public boolean validate(Genome genome) {
        for (NodeGene node : genome.getNodes().values()) {
            if (!validateNode(genome, node))
                return false;
        }

        return true;
    }

    private boolean validateNode(Genome genome, NodeGene node) {
        switch (node.getType()) {
            case HIDDEN:
                return validateHiddenNode(genome, node.getId());

            case OUTPUT:
                return validateOutputNode(genome, node.getId());

            case INPUT:
                return validateInputNode(genome, node.getId());
        }

        throw new RuntimeException("Could not validate node of unknown type: " + node.getType());
    }

    private boolean validateHiddenNode(Genome genome, int nodeId) {
        return hasNodeIncoming(genome, nodeId) && hasNodeOutgoing(genome, nodeId);
    }

    private boolean validateOutputNode(Genome genome, int nodeId) {
        return !hasNodeOutgoing(genome, nodeId);
    }

    private boolean validateInputNode(Genome genome, int nodeId) {
        return !hasNodeIncoming(genome, nodeId);
    }

    private boolean hasNodeOutgoing(Genome genome, int nodeId) {
        return !genome.getOutConnections(nodeId).isEmpty();
    }

    private boolean hasNodeIncoming(Genome genome, int nodeId) {
        return !genome.getInConnections(nodeId).isEmpty();
    }
}
