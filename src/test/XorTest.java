package test;

import main.Neat;
import main.evaluator.SimpleEvaluator;
import main.generation.Generation;
import main.genome.Genome;
import main.genome.GenomeEvaluator;
import main.genome.GenomePropagator;
import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Arrays;
import java.util.List;

public class XorTest {
    private final static int MAX_GENERATIONS = 300;
    private final GenomeEvaluator evaluator = new SimpleEvaluator() {
        private final List<List<Float>> input = Arrays.asList(
                Arrays.asList(1.0f, 1.0f),
                Arrays.asList(1.0f, 0.0f),
                Arrays.asList(0.0f, 1.0f),
                Arrays.asList(0.0f, 0.0f)
        );

        private final List<Float> expectedOutput = Arrays.asList(
                0.0f,
                1.0f,
                1.0f,
                0.0f
        );

        @Override
        public float evaluate(Genome genome) {
            GenomePropagator propagator = new GenomePropagator(genome);

            float fitness = 4;
            for (int i = 0; i < input.size(); i++) {
                Float result = propagator.propagate(input.get(i)).getFirst();
                fitness -= (float) Math.pow((result - expectedOutput.get(i)), 2);
            }

            return fitness;
        }
    };

    @Test
    public void runXOR() {
        Neat neat = Neat.create(evaluator, 2, 1);
        neat.getSpeciesFactory().setDistanceThreshold(10);
        neat.getGenerationFactory().setPopulationSize(150);

        float maxFitness = 0;

        // Loop through generations until we have a result, or give up.
        Generation generation = null;
        for (int i = 1; i < MAX_GENERATIONS; i++) {
            generation = neat.evolve();

            // Assert generation number.
            assertThat(generation.getGenerationNumber(), is(i));

            Genome bestGenome = generation.getBestGenome();

            if (bestGenome.getFitness() < maxFitness)
                fail("Fitness decreased...");

            maxFitness = bestGenome.getFitness();
            if (bestGenome.getFitness() == 4.0f) {
                System.out.println("Finished in : " + i + " generations");
                break;
            }
        }

        Genome bestGenome = generation.getBestGenome();
        GenomePropagator p = new GenomePropagator(bestGenome);

        assertThat(round(p.propagate(Arrays.asList(1.0f, 1.0f)).getFirst()), is(0.0f));
        assertThat(round(p.propagate(Arrays.asList(1.0f, 0.0f)).getFirst()), is(1.0f));
        assertThat(round(p.propagate(Arrays.asList(0.0f, 1.0f)).getFirst()), is(1.0f));
        assertThat(round(p.propagate(Arrays.asList(0.0f, 0.0f)).getFirst()), is(0.0f));
    }

    /**
     * Round to 3 decimals.
     */
    private float round(float v) {
        return (float)Math.round(v * 1000.0f) / 1000.0f;
    }
}
